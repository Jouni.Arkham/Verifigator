** Change Log **
Version 1.0.0
* Fail method when error

Version 0.0.5
* Update with Verifigator API

Version 0.0.3
* Fixed validate on submit event.
* Changed method to set element to validate and display error.

Version 0.0.2
* Specify form and input elements, create sample page when plugin activate

Version 0.0.1
* First version
