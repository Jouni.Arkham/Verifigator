/*!
 * jQuery Verifigator Validate plugin 
 * Version 1.0.0
 * Original author: Asadong Pongpanich
 * Licensed under the MIT license
 * 
 */

 
(function ( $ ) {

    $.fn.Verifigator = function( options ) {
        
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.

            apikey: "",
            requires:{
                full_name: "",
                email_address: "",
                first_name: "",
                last_name:""
            },
            errors:{
                main: null,
                full_name: null,
                email_address: null,
                first_name: null,
                last_name: null
            },
            error_msg:{
                main: "Please enter your real information.",
                full_name: "Please enter your full name (first name and last name.)",
                email_address: "Please fill in email address.",
                first_name: "Please enter your first name.",
                last_name: "Please enter your last name."
            },
            minimum_quality: 4,
            require_full_name: 1,
            accept_corporate_name: 1,
            accept_role_email_account: 1,
            accept_free_email_account: 1,
            accept_disposable_email_account: 0,
            accept_spamtrap_email_account: 0,
            accept_all_if_error: 0,
            debug:0,
            done: null,
            fail: null,
            version: '1.0.0'


        }, options );

        var is_valid = 0;
        var idle = 1;
        var current = {};
        var inputPrefix = "verifigator-input-";
        var form = this;
        
        var get_type_by_class = function (obj){
            var classes = obj.attr('class').split(' ');

            for (var i=0; i < classes.length; i++ ) {
                if (classes[i].substring(0, inputPrefix.length) == "verifigator-input-") {
                    return classes[i].substring(inputPrefix.length, classes[i].length); 
                }
            }
        };

        var callback_value = function(error_item, error_message){
            current[error_item] = error_message;
            //settings.done(current);
        };

        var verifigator_ajx_call = function(e){
            //console.log(settings.errors.length);
            
           
            if ( is_valid == false ) {
                e.preventDefault();
            } 

            //$("span.verifigator_error").remove();
            var dataString = {};

            dataString.email_address = $("input[name='"+settings.requires.email_address+"']").val();
            dataString.full_name = $("input[name='"+settings.requires.full_name+"']").val();
            dataString.first_name = $("input[name='"+settings.requires.first_name+"']").val();
            dataString.last_name = $("input[name='"+settings.requires.last_name+"']").val();
            dataString.api_key = settings.apikey;

    
            var request = $.ajax({
                method: 'GET',
                url: 'https://api.verifigator.com/v1/leads/validate/',
                crossDomain: true,
                contentType: 'application/x-www-form-urlencoded',
                data: dataString,
                dataType: 'json'

            });

            request.fail(function( jqXHR, textStatus, errorThrown ) {

                if (typeof settings.fail == 'function') {

                    settings.fail({
                        status_code: jqXHR.status,
                        error_message: jqXHR.responseText,
                        version: settings.version
                    });
                }
            });
            
            request.done(function(data){
                current = data;
                current.version = settings.version;
                idle = 0;

                var valid = true;


                if ( typeof data.success == "undefined"){
                    valid = false;
                }

                form.find(".verifigator-error").html("");

                // If main error is not exists, add it.
                if ( $("#"+settings.errors.main).length === 0 ){
                    form.prepend('<p id="'+settings.errors.main+'" class="verifigator-error"></p>');
                }
                
                if ( data.success == 0 && settings.accept_all_if_error == 0 ){
                    //callback_value("error_valid", data.error_message);
                    valid = false;
                }

                // Maximun Quality
                if ( data.quality_rating < settings.minimum_quality ){
                    if ( settings.errors.main ) {
                        verifigator_error_notify($("#"+settings.errors.main), settings.error_msg.main);
                    }
                    //callback_value("Please enter your real information.");
                    valid = false;
                }

                // Require Full Name
                if ( dataString.full_name === "" && settings.requires.full_name ){
                    if ( settings.errors.full_name ) {
                        if ( $("#"+settings.errors.full_name).length === 0 ){
                            $("#"+settings.requires.full_name).append('<span id="'+$("#"+settings.errors.full_name)+'"><span>');
                        }
                        verifigator_error_notify($("#"+settings.errors.full_name), settings.error_msg.full_name);
                    }
                    valid = false;
                }

                // Require First Name
                if ( dataString.first_name === "" && settings.requires.first_name ){
                    if ( settings.errors.first_name ) {
                        verifigator_error_notify($("#"+settings.errors.first_name), settings.error_msg.first_name);
                    } else {
                        verifigator_error_notify($("#"+settings.errors.main), settings.error_msg.first_name);
                    }
                    valid = false;
                }

                // Require First Name
                if ( dataString.last_name === "" && settings.requires.last_name ){
                    if ( settings.errors.last_name ) {
                        verifigator_error_notify($("#"+settings.errors.last_name), settings.error_msg.last_name);
                    } else {
                        verifigator_error_notify($("#"+settings.errors.main), settings.error_msg.last_name);
                    }
                    valid = false;
                }

            
                // Valid email
                if ( !data.valid ){
                    if ( settings.errors.email_address ) {
                        verifigator_error_notify($("#"+settings.errors.email_address), settings.error_msg.email_address);
                    }
                    valid = false;
                    
                } else {

                    // Accept Coporate Name
                    if ( data.user_coporatename && !settings.accept_corporate_name ){
                        if ( settings.errors.email_address ) {
                            verifigator_error_notify($("#"+settings.errors.email_address), "Please enter your personal details, not corporate information.");
                        }
                        valid = false;
                    }

                    // Valid free account
                    if ( data.free_account && !settings.accept_free_email_account ){
                        if ( settings.errors.email_address ) {
                            verifigator_error_notify($("#"+settings.errors.email_address), "We do not accept free email addresses. Please use another email address.");
                        }
                        valid = false;
                    }

                    // Role Account
                    if ( data.role_account && !settings.accept_role_email_account ){
                        if ( settings.errors.email_address ) {
                            verifigator_error_notify($("#"+settings.errors.email_address), "Please enter your personal email address, we do not accept role account email addresses.");
                        }
                        valid = false;
                    }

                    // Disposable
                    if ( data.disposable && !settings.accept_disposable_email_account ){
                        if ( settings.errors.email_address ) {
                            verifigator_error_notify($("#"+settings.errors.email_address), "We do not accept disposable email addresses. Please use another email address.");
                        }
                        valid = false;
                    }

                    // Disposable
                    if ( data.spamtrap && !settings.accept_spamtrap_email_account ){
                        if ( settings.errors.email_address ) {
                            verifigator_error_notify($("#"+settings.errors.email_address), "We do not accept this email address. Please use another email address.");
                        }
                        valid = false;
                    }
                }

                is_valid = valid;

                if (typeof settings.done == 'function' && is_valid) {
                    settings.done(current);
                    //console.log(current);
                }

                return valid;



            });

        };

        function verifigator_error_notify(obj, message){

            if ( obj ) {
                obj.html("");
            }

            obj.addClass("verifigator-error");
            obj.html(message);
            if (typeof settings.fail == 'function' ) {
                settings.fail({success: 0, error_message: message});
            }
        }

        //Initialize
        function initialize() {
            //form = $(this);
            //console.log(form);
            form.addClass("verifigator-validate");
            var myInterval = null;
            if ( settings.debug == 0 ) {
                myInterval = setInterval(function(){
                    if ( is_valid == true ){
                        form.submit();
                        clearInterval(myInterval);
                    }
                }, 1000);
            }
            
        }

        //Return Plugin
        return this.each(function(){
            initialize();
            form.on("submit", verifigator_ajx_call);
        });

    };

}( jQuery ));