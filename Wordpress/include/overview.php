<?php

/*
** Overview Page
*/

?>
<div class="wrap">
	<h1>Overview</h1>
	<div class="postbox" style="padding:10px;">
		<h2>Welcome to Verifigator WordPress plugin!</h2>
		<p>This plugin allows you to use Verifigator's email and lead validation service with any existing subscription or signup form in your WordPress website.</p>
		<p>To be able to use the plugin, you must have a Verifigator API key. If you don't have one, you can get an API key by signing up for a free account at <a href="https://verifigator.com" target="_blank">www.verifigator.com</a>.
		<br/>After you have your API key, you can start by defining which form in your website you wish to protect by Verifigator by <a href="https://verifigator.com/wp-admin/post-new.php?post_type=vfg-form">creating a new validation item</a>.</p>
	</div>
</div>