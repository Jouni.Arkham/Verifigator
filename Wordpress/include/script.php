<?php
// JavaScript Minifier
function minify_js($input) {
    if(trim($input) === "") return $input;
    return preg_replace(
        array(
            // Remove comment(s)
            '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
            // Remove white-space(s) outside the string and regex
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
            // Remove the last semicolon
            '#;+\}#',
            // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
            '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
            // --ibid. From `foo['bar']` to `foo.bar`
            '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
        ),
        array(
            '$1',
            '$1$2',
            '}',
            '$1$3',
            '$1.$3'
        ),
    $input);
}

function create_js_scripts() {
	$scripts = '
<script type="text/javascript">
/*
    Loading Function
*/

var the_Loading = function(){
    var loadingTimeout = null;
    
    return {
        // Start to set Timeout to hide loading
        process : function(){
            loadingTimeout = setTimeout(function(){ jQuery("div.the-loading").fadeOut(800); }, 2000); 
        }, 
        start : function(){
            jQuery("div.the-loading").fadeIn(0);
        },
        // Hide Loading Immediately
        stop : function(){
            jQuery("div.the-loading").fadeOut(800);
        },
        // Clear Timeout for hiding loading
        pause : function(){
            if ( loadingTimeout !== null ) {
                clearTimeout(loadingTimeout);
            }
        },
        // Restart Loading 
        restart : function(){
            jQuery("div.the-loading").fadeIn(0);
            //this.process()
        }
    };
}();'."\r\n";
	
	$scripts .= '
		var options = {
			verifigator_public_API_key : "'.esc_attr( get_option('verifigator_public_API_key') ).'"
			,firstname_default_error : "'.esc_attr( get_option('firstname_default_error') ).'"
			,main_default_error : "'.esc_attr( get_option('main_default_error') ).'"
			,lastname_default_error : "'.esc_attr( get_option('lastname_default_error') ).'"
			,fullname_default_error : "'.esc_attr( get_option('fullname_default_error') ).'"
			,email_default_error : "'.esc_attr( get_option('email_default_error') ).'"
		}; '."\r\n";
		
			// Get All Forms to create js configure
			$args = array(
			  'numberposts' => -1,
			  'post_status' => 'publish',
 			  'post_type'   => 'vfg-form');
			 $verifigator_get_posts = get_posts( $args );
			 //print_r($verifigator_get_posts);
		
		$scripts .= 'jQuery(document).ready(function($){ '."\r\n";
			$scripts .= 'if ( jQuery("div.the-loading").length === 0 ){
        jQuery("body").prepend(\'<div class="the-loading"><p><img src="'.plugin_dir_url(__DIR__).'images/message_loading.svg" /></p></div>\');
    }';
			foreach ($verifigator_get_posts as $key => $value) {

				$get_vfg_form_name = get_post_meta( $value->ID,'vfg_form_name' , true );
				$get_vfg_form_ajax = get_post_meta( $value->ID,'vfg_form_ajax' , true );
				$vfg_form_redirect = get_post_meta( $value->ID,'vfg_form_redirect' , true );
				$accept_quality_rate = get_post_meta( $value->ID,'accept_quality_rate' , true );
				if( empty( $accept_quality_rate ) ) $accept_quality_rate = 4;
				$accept_role_account = get_post_meta( $value->ID,'accept_role_account' , true );
				if( $accept_role_account == '' ) $accept_role_account = 1;
				$get_first_name = get_post_meta( $value->ID,'first_name' , true );
				$get_last_name = get_post_meta( $value->ID,'last_name' , true );
				$get_full_name = get_post_meta( $value->ID,'full_name' , true );
				$get_email = get_post_meta( $value->ID,'email' , true );

				//$get_vfg_form_name = get_post_meta( $value->ID,'vfg_form_name' , true );
				$get_main_error = get_post_meta( $value->ID,'main_error' , true );
				$get_first_name_error = get_post_meta( $value->ID,'firstname_error' , true );
				$get_last_name_error = get_post_meta( $value->ID,'lastname_error' , true );
				$get_full_name_error = get_post_meta( $value->ID,'fullname_error' , true );
				$get_email_error = get_post_meta( $value->ID,'email_error' , true );

				$get_main_error_msg = get_post_meta( $value->ID,'main_error_msg' , true );
				$get_main_success_msg = get_post_meta( $value->ID,'main_success_msg' , true );
				$get_first_name_error_msg = get_post_meta( $value->ID,'firstname_error_msg' , true );
				$get_last_name_error_msg = get_post_meta( $value->ID,'lastname_error_msg' , true );
				$get_full_name_error_msg = get_post_meta( $value->ID,'fullname_error_msg' , true );
				$get_email_error_msg = get_post_meta( $value->ID,'email_error_msg' , true );
				

				if( !empty( $get_vfg_form_name ) ) { 
				$scripts .=  '//'.$get_vfg_form_name."\r\n";
				if ( $get_vfg_form_ajax == 1 ){
					$scripts .= '
						$("#'.$get_vfg_form_name.'").on("submit", function(e){
							e.preventDefault();
							the_Loading.start();
						});'."\r\n";
				}

				$scripts .= '	$("#'.$get_vfg_form_name.'").Verifigator({
						apikey: options.verifigator_public_API_key,
						minimum_quality: '.$accept_quality_rate.',
						accept_role_email_account:'.$accept_role_account.',
						requires:{';
					
							if( !empty( $get_first_name ) ) { $scripts .= "first_name: '".$get_first_name."',\r\n"; }
							if( !empty( $get_last_name ) ) { $scripts .= "last_name: '".$get_last_name."',\r\n"; }
							if( !empty( $get_full_name ) ) { $scripts .= "full_name: '".$get_full_name."',\r\n"; }
							if( !empty( $get_email ) ) { $scripts .= "email_address: '".$get_email."',\r\n"; }
					
					$scripts .= '},
						errors:{' ;
							
								if( !empty( $get_main_error ) ) { $scripts .= "main: '$get_main_error',\r\n"; }
							    if( !empty( $get_first_name_error ) ) { $scripts .= "first_name: '$get_first_name_error',\r\n"; }
								if( !empty( $get_last_name_error ) ) { $scripts .= "last_name: '$get_last_name_error',\r\n"; }
								if( !empty( $get_full_name_error ) ) { $scripts .= "full_name: '$get_full_name_error',\r\n"; }
								if( !empty( $get_email_error ) ) { $scripts .= "email_address: '$get_email_error',\r\n"; }
							
						$scripts .= '},';
						
								$error_msg = "";

								if( !empty( $get_main_error_msg ) ) { $error_msg .= "main: '$get_main_error_msg',\r\n"; } elseif( !empty( get_option('main_default_error') ) ) { $error_msg .= "main: options.main_default_error,\r\n"; }
							    if( !empty( $get_first_name_error_msg ) ) { $error_msg .= "first_name: '$get_first_name_error_msg',\r\n"; } elseif( !empty( get_option('firstname_default_error') ) ) { $error_msg .= "first_name: options.firstname_default_error,\r\n"; }
								if( !empty( $get_last_name_error_msg ) ) { $error_msg .= "last_name: '$get_last_name_error_msg',\r\n"; } elseif( !empty( get_option('lastname_default_error') ) ) { $error_msg .= "last_name: options.lastname_default_error,\r\n"; }
								if( !empty( $get_full_name_error_msg ) ) { $error_msg .= "full_name: '$get_full_name_error_msg',\r\n"; } elseif( !empty( get_option('fullname_default_error') ) ) { $error_msg .= "full_name: options.fullname_default_error,\r\n"; }
								if( !empty( $get_email_error_msg ) ) { $error_msg .= "email_address: '$get_email_error_msg',\r\n"; } elseif( !empty( get_option('email_default_error') ) ) { $error_msg .= "email_address: options.email_default_error,\r\n"; }
								if ( !empty($error_msg) ){
									$scripts .= "\r\n".'error_msg:{'.$error_msg.'},';
								}

						$scripts .= '
						done: function(data){ ';
							//Send Form by using Ajax
							//$scripts .= 'console.log(data); return;';
								if ( $get_vfg_form_ajax == 1 ){
									$scripts .= 'if ( $("#'.$get_main_error.'").hasClass("text-success") ){
							    		$("#'.$get_main_error.'").removeClass("text-success"); 
							    		$("#'.$get_main_error.'").addClass("text-danger"); }';
									$scripts .= 'if ( data.valid === 1 && data.quality_rating >= '.$accept_quality_rate.' ) {'."\r\n";
									$scripts .= 'var ajax_action = $("#'.$get_vfg_form_name.'").attr("action");'."\r\n";
									$scripts .= 'var dataString = $("#'.$get_vfg_form_name.'").serialize();'."\r\n";
									$scripts .= 'dataString = dataString+"&action="+ajax_action;'."\r\n";
									$scripts .= 'dataString = dataString+"&'.$get_email.'="+data.email'."\r\n";
									//$scripts .= 'console.log(dataString);'."\r\n";
									$scripts .= '$("#'.$get_vfg_form_name.'").on("submit", function(event){';
										$scripts .= 'event.preventDefault();'."\r\n";
										$scripts .= '$.ajax({
											    url: frontend_ajax_object.ajaxurl,
											    method: "post",
											    data: dataString,
											    success: function(data){ 
											    	//console.log(data);
											    	if ( data.success === 1 ){
												    	$("#'.$get_main_error.'").removeClass("text-danger"); 
												    	$("#'.$get_main_error.'").addClass("text-success");
												    	$("#'.$get_main_error.'").html("'.$get_main_success_msg.'");
												    	$("#'.$get_vfg_form_name.'")[0].reset();';
												    	//Redirect when redirect url is set.
														if ( !empty($vfg_form_redirect)){
													    	$scripts .=  "\r\n".'document.location.href = "'.$vfg_form_redirect.'";';
													    	//echo "\r\n".'return;';
													    }
													$scripts .= '} else {
														$("#'.$get_main_error.'").addClass("text-danger");
												    	$("#'.$get_main_error.'").html(data.error_message);
													}';
												    $scripts .= 'the_Loading.stop();';
											$scripts .= '    },
											    error: function(jqXHR, status){
											    	$("#'.$get_main_error.'").addClass("text-danger");
											    	$("#'.$get_main_error.'").html(jqXHR.statusText);';
											    	$scripts .= 'the_Loading.stop();';
											
											$scripts .= '
											    }
											});';
									$scripts .= '});';
									//echo 'console.log(dataString);'."\r\n";
									$scripts .= '}'."\r\n";
									$scripts .= 'else {';
										$scripts .= '$("#'.$get_main_error.'").html("Invalid data, please fill in valid data.");';
										$scripts .= 'the_Loading.stop();
									}';
								} else {
									$scripts .= 'window.location.reload();';
									$scripts .= 'return;';
								}
						
						$scripts .= ' },
						fail: function(data){
								the_Loading.stop();
							},
						debug: 0
					});'."\r\n";
				
				}

		 	}
		$scripts .= '
		
		});
	</script>';
	echo '<!-- Verifigator Wordpress Validate Script Begin -->'."\r\n";
	echo minify_js($scripts)."\r\n";
	echo '<!-- Verifigator Wordpress Validate Script End -->'."\r\n";
}