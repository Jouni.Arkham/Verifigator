<?php
	function verifigator_form_shortcode($attr, $content){
    extract(shortcode_atts(array(
      'name' => 'frm_verifigator',
      'id' => 'frm_verifigator',
      'action' => '#'
    ), $attr));

    $form = '<form name="'.$name.'" id="'.$id.'" action="'.$action.'" method="post">'."\r\n";
    //$form .= '<p id="frm_main_error" class="text-danger"></p>'."\r\n";
    //$form .= '<input type="text" name="frm_fullname" id="frm_fullname" value=""><span id="frm_fullname_error"></span>'."\r\n";
    $form .= '<p><input type="text" name="frm_first_name" id="frm_first_name" value="" placeholder="First Name"><br /><span id="frm_first_name_error" class="text-danger"></span></p>'."\r\n";
    $form .= '<p><input type="text" name="frm_last_name" id="frm_last_name" value="" placeholder="Last Name"><br /><span id="frm_last_name_error" class="text-danger"></span></p>'."\r\n";
    $form .= '<p><input type="text" name="frm_email" id="frm_email" value="" placeholder="Email Address"><br /><span id="frm_email_error" class="text-danger"></span></p>'."\r\n";
    $form .= '<p><button type="submit">Submit</button></p>'."\r\n";
    $form .= '</form>'."\r\n";
    return $form;
  }
  add_shortcode('verifigator-demo-form', 'verifigator_form_shortcode');
?>