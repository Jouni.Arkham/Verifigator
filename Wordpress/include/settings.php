<?php

//register API key settings
register_setting( 'verifigator-validate-apikey-setting', 'verifigator_public_API_key' );
add_settings_section("verifigator_public_API_key", "API Key Option. ", 'section_callback', "verifigator-validate-apikey-setting");
add_settings_field( 'verifigator_public_API_key', 'Verfigator API Key: ', 'field_callback', 'verifigator-validate-apikey-setting', 'verifigator_public_API_key' );
//register Main Error settings
register_setting( 'verifigator-validate-error-setting', 'main_default_error' );
add_settings_section("main_default_error", "Error messages Option. ", 'section_firstname_error', "verifigator-validate-error-setting");
add_settings_field( 'main_default_error', 'Main Error messages : ', 'main_callback_error', 'verifigator-validate-error-setting', 'main_default_error' );
//register Firstname Error settings
register_setting( 'verifigator-validate-error-setting', 'firstname_default_error' );
add_settings_section("firstname_default_error", " ", 'section_callback_error', "verifigator-validate-error-setting");
add_settings_field( 'firstname_default_error', 'Firstname Error messages : ', 'firstname_callback_error', 'verifigator-validate-error-setting', 'firstname_default_error' );
//register Lastname Error settings
register_setting( 'verifigator-validate-error-setting', 'lastname_default_error' );
add_settings_section("lastname_default_error", "", 'section_callback_error', "verifigator-validate-error-setting");
add_settings_field( 'lastname_default_error', 'Lastname Error messages : ', 'lastname_callback_error', 'verifigator-validate-error-setting', 'lastname_default_error' );
//register Fullname Error settings
register_setting( 'verifigator-validate-error-setting', 'fullname_default_error' );
add_settings_section("fullname_default_error", "", 'section_callback_error', "verifigator-validate-error-setting");
add_settings_field( 'fullname_default_error', 'Fullname Error messages : ', 'fullname_callback_error', 'verifigator-validate-error-setting', 'fullname_default_error' );
//register Email Error settings
register_setting( 'verifigator-validate-error-setting', 'email_default_error' );
add_settings_section("email_default_error", " ", 'section_callback_error', "verifigator-validate-error-setting");
add_settings_field( 'email_default_error', 'Email Error messages : ', 'email_callback_error', 'verifigator-validate-error-setting', 'email_default_error' );



function section_callback( $arguments ) {
    echo "Please fill in your API key to configure Verifigator Validate.";
}
function section_firstname_error( $arguments ) {
    echo "Please fill in your default error messages to configure Verifigator Validate.";
}
function section_callback_error( $arguments ) {
    echo "";
}

function main_callback_error(){
echo '<input type="text" name="main_default_error" value="'.esc_attr( get_option('main_default_error') ).'" />';
}
function field_callback(){
    echo '<input type="text" name="verifigator_public_API_key" value="'.esc_attr( get_option('verifigator_public_API_key') ).'" />';

} 
function firstname_callback_error(){
echo '<input type="text" name="firstname_default_error" value="'.esc_attr( get_option('firstname_default_error') ).'" />';
}
function lastname_callback_error(){
echo '<input type="text" name="lastname_default_error" value="'.esc_attr( get_option('lastname_default_error') ).'" />';
}
function fullname_callback_error(){
echo '<input type="text" name="fullname_default_error" value="'.esc_attr( get_option('fullname_default_error') ).'" />';
}
function email_callback_error(){
echo '<input type="text" name="email_default_error" value="'.esc_attr( get_option('email_default_error') ).'" />';
}


//Default Value
if ( get_option( 'main_default_error' ) === false ) // Nothing yet saved
    update_option( 'main_default_error', 'Please enter your real information.' );

if ( get_option( 'firstname_default_error' ) === false ) // Nothing yet saved
    update_option( 'firstname_default_error', 'Please fill in your first name.' );

if ( get_option( 'lastname_default_error' ) === false ) // Nothing yet saved
    update_option( 'lastname_default_error', 'Please fill in your last name.' );

if ( get_option( 'fullname_default_error' ) === false ) // Nothing yet saved
    update_option( 'fullname_default_error', 'Please fill in your full name.' );

if ( get_option( 'email_default_error' ) === false ) // Nothing yet saved
    update_option( 'email_default_error', 'Please fill in your email address.' );


//save option
if ( isset($_POST['verifigator_public_API_key'])){
    update_option('verifigator_public_API_key', $_POST['verifigator_public_API_key']);
} 

if ( isset($_POST['firstname_default_error']) ){
    update_option('firstname_default_error', $_POST['firstname_default_error']);
}
if ( isset($_POST['lastname_default_error']) ){
    update_option('lastname_default_error', $_POST['lastname_default_error']);
}
if ( isset($_POST['fullname_default_error'])){
    update_option('fullname_default_error', $_POST['fullname_default_error']);
}
if ( isset($_POST['email_default_error']) ){
    update_option('email_default_error', $_POST['email_default_error']);
}
if ( isset($_POST['main_default_error']) ){
    update_option('main_default_error', $_POST['main_default_error']);
}

?>

<div class="wrap">
    <h1>Settings</h1>

    <?php
if( isset( $_GET[ 'tab' ] ) ) {
    $active_tab = $_GET[ 'tab' ];
} 
else{
	 $active_tab = 'API_key';
}
?>

    <h2 class="nav-tab-wrapper">
            <a href="edit.php?post_type=vfg-form&page=verifigator-validate-apikey-setting&tab=API_key" class="nav-tab <?php echo $active_tab == 'API_key' ? 'nav-tab-active' : ''; ?>">API Settings</a>
            <a href="edit.php?post_type=vfg-form&page=verifigator-validate-apikey-setting&tab=Error_messages" class="nav-tab <?php echo $active_tab == 'Error_messages' ? 'nav-tab-active' : ''; ?>">Error messages </a>
        </h2>

    <form method="post">

        <?php 
        if( $active_tab == 'API_key' ) {
        settings_fields( 'verifigator-validate-apikey-setting' ); 
         do_settings_sections( 'verifigator-validate-apikey-setting' ); 
     }else {

         settings_fields( 'verifigator-validate-error-setting' ); 
         do_settings_sections( 'verifigator-validate-error-setting' ); 
	      }
         ?>


        <?php submit_button(); ?>
    </form>
</div>


 