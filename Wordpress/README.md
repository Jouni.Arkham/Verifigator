***Changed Logs
Version 1.0.4
- Change the quality rating more than 5.
Version 1.0.3
- Add Quality Rating and Accpeted the role account.
Version 1.0.2
- Add Loading when form is submmited.
Version 1.0.1
- Add redirect URL for redirect to after form completed submission. It will not set in script, if user does not set form setting in back-end.
- Minify JS scripts.