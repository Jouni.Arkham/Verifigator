<?php
/**
 * Plugin Name: Verifigator Plugin
 * Plugin URI: https://verifigator.com/plugins
 * Description: Verifigator Wordpress Plugin, to manage submit form
 * Version: 1.0.4
 * Author: Arkham Enterprise
 * Author URL: https://verifigator.com
 */

//Add Shortcode of demo form
include(plugin_dir_path(__FILE__)."include/shortcode.php");

$handle = 'jquery';
$list = 'enqueued';
if (wp_script_is( $handle, $list )) {
	return;
} else {
	wp_enqueue_script( 'jquery' );
}


if (wp_script_is( "verifigator-jquery", "enqueued" )) {
	return;
} else {
	wp_register_script( 'jQuery-Verifigator', plugin_dir_url(__FILE__).'scripts/jQuery.Verifigator.js');
	wp_enqueue_script( 'jQuery-Verifigator' );
}


wp_register_style( 'jQuery-Verifigator-Style', plugin_dir_url(__FILE__).'styles/jQuery.Verifigator.css');
wp_enqueue_style( 'jQuery-Verifigator-Style' );

add_action( 'admin_menu', 'Verifigator_menu' );
function Verifigator_menu() {
	add_submenu_page( 'edit.php?post_type=vfg-form', 'Overview', 'Overview', 'read', 'verifigator-form', 'verifigator_overview' );
	add_submenu_page( 'edit.php?post_type=vfg-form', 'Settings', 'Settings', 'manage_options', 'verifigator-validate-apikey-setting', 'Setting_layout' );
}

function verifigator_overview(){
	include("include/overview.php");
}

function vfg_form_post_types() {
	$labels = array(
		'name'               => 'Verifigator Validate',
		'singular_name'      => 'Verifigator Validate',
		'menu_name'          => 'Verifigator Validate',
		'name_admin_bar'     => 'Verifigator Validate',
		'add_new'            => 'Add New Form',
		'add_new_item'       => 'Add New Form',
		'new_item'           => 'New Form',
		'edit_item'          => 'Edit Form',
		'view_item'          => 'View Form',
		'all_items'          => 'All Forms',
		'search_items'       => 'Search Verifigator Validate',
		'parent_item_colon'  => 'Parent Verifigator Validate:',
		'not_found'          => 'No Validate found.',
		'not_found_in_trash' => 'No Validate found in Trash.'
	);

	$args = array( 
		'public'      => true, 
		'labels'      => $labels,
		'has_archive' => false,
		'show_in_menu' => true,
		'publicly_queryable' => false,
		'supports' => array(
			'title'
		)
	);


    	register_post_type( 'vfg-form', $args );
}
add_action( 'init', 'vfg_form_post_types' );


// Staff Division Meta Box
class vfg_form_Meta_Box {

public function __construct() {
//echo "test";
if ( is_admin() ) {
add_action( 'load-post.php', array( $this, 'init_metabox' ) );
add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
}	
}

public function init_metabox() {
	
add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );	
add_action( 'save_post', array( $this, 'save_metabox' ), 10, 2 );

}

public function add_metabox() {
	add_meta_box( 'vfg_form_name', __( 'Verifigator Validation Form', 'vfg' ), array( $this, 'render_metabox' ), 'vfg-form', 'advanced','default');
}

/*public function add_firstname_metabox() {
	
add_meta_box(
			
'first_name',
			
__( 'First Name Field', 'vfg' ), 
array( $this, 'render_metabox' ),
			
'vfg-form',	
'advanced',			
'default'

);
}

public function add_lastname_metabox() {
	
add_meta_box(
			
'last_name',
			
__( 'Last name Field', 'vfg' ), 
array( $this, 'render_metabox' ),
			
'vfg-form',	
'advanced',			
'default'

);
}

public function add_fullname_metabox() {
	
add_meta_box(
			
'full_name',
			
__( 'Full Name Field', 'vfg' ), 
array( $this, 'render_metabox' ),
			
'vfg-form',	
'advanced',			
'default'

);
}

public function add_email_metabox() {
	
add_meta_box(
			
'email',
			
__( 'Email Field', 'vfg' ), 
array( $this, 'render_metabox' ),
			
'vfg-form',	
'advanced',			
'default'

);
}*/
	
public function render_metabox( $post ) {

//print_r( $post);
// Add nonce for security and authentication.
wp_nonce_field( 'vfg_form_nonce_action', 'vfg_form_nonce' );

// Retrieve an existing value from the database.	
$vfg_form_name = get_post_meta( $post->ID, 'vfg_form_name', true );
$vfg_form_ajax = get_post_meta( $post->ID, 'vfg_form_ajax', true );
$vfg_form_redirect = get_post_meta( $post->ID, 'vfg_form_redirect', true );
$accept_quality_rate = get_post_meta( $post->ID, 'accept_quality_rate', true );
$accept_role_account = get_post_meta( $post->ID, 'accept_role_account', true );
$main_error = get_post_meta( $post->ID, 'main_error', true );
$main_success_msg = get_post_meta( $post->ID, 'main_success_msg', true );
$main_error_msg = get_post_meta( $post->ID, 'main_error_msg', true );
$first_name = get_post_meta( $post->ID, 'first_name', true );
$firstname_error = get_post_meta( $post->ID, 'firstname_error', true );
$first_name_error_msg = get_post_meta( $post->ID, 'first_name_error_msg', true );
$last_name = get_post_meta( $post->ID, 'last_name', true );
$lastname_error = get_post_meta( $post->ID, 'lastname_error', true );
$last_name_error_msg = get_post_meta( $post->ID, 'last_name_error_msg', true );
$full_name = get_post_meta( $post->ID, 'full_name', true );
$fullname_error = get_post_meta( $post->ID, 'fullname_error', true );
$fullname_error_msg = get_post_meta( $post->ID, 'fullname_error_msg', true );
$email = get_post_meta( $post->ID, 'email', true );
$email_error = get_post_meta( $post->ID, 'email_error', true );
$email_error_msg = get_post_meta( $post->ID, 'email_error_msg', true );

// Set default values.
if( empty( $vfg_form_name ) ) $vfg_form_name = '';
if( empty( $vfg_form_ajax ) ) $vfg_form_ajax = 0;
if( empty( $vfg_form_redirect ) ) $vfg_form_redirect = null;
if( empty( $accept_quality_rate ) ) $accept_quality_rate = 6;
if( $accept_role_account == '' ) $accept_role_account = 1;
if( empty( $main_error ) ) $main_error = '';
if( empty( $main_error_msg ) ) $main_error_msg = '';
if( empty( $first_name ) ) $first_name = '';
if( empty( $firstname_error ) ) $firstname_error = '';
if( empty( $first_name_error_msg ) ) $first_name_error_msg = '';
if( empty( $last_name ) ) $last_name = '';
if( empty( $lastname_error ) ) $lastname_error = '';
if( empty( $last_name_error_msg ) ) $last_name_error_msg = '';
if( empty( $full_name ) ) $full_name = '';
if( empty( $fullname_error ) ) $fullname_error = '';
if( empty( $fullname_error_msg ) ) $fullname_error_msg = '';
if( empty( $email ) ) $email = '';
if( empty( $email_error ) ) $email_error = '';
if( empty( $email_error_msg ) ) $email_error_msg = '';	

// Form Name.	
echo '<table class="form-table">';
echo '<tr>';
echo '<th><label for="vfg_form_name" class="vfg_form_label">' . __( 'Form Name:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="vfg_form_name" name="vfg_form_name" class="vfg_form_field" value="'.$vfg_form_name.'" >';
echo '<p><small>Please fill in the name of form which you want to validate.</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="vfg_form_ajax" class="vfg_form_label">' . __( 'Ajax:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="checkbox" id="vfg_form_ajax" name="vfg_form_ajax" class="vfg_form_field" value="1"'; if ( $vfg_form_ajax == 1 ) echo ' checked="checked"'; echo '> Submit form by using Ajax.';
echo '<p><small>Please check this option if you want to send this form by Ajax.</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="vfg_form_redirect" class="vfg_form_redirect">' . __( 'Redirect URL:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="vfg_form_redirect" name="vfg_form_redirect" class="vfg_form_field" value="'.$vfg_form_redirect.'"  style="width:100%">';
echo '<p><small>Please fill in the full URL for redirect to after form completed submission. For example: https://verifigator.com/my-account (optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="accept_quality_rate" class="accept_quality_rate">' . __( 'Minimum Quality Rating', 'vfg' ) . ':</label></th>';
echo '<td>';
echo '<input type="number" max="10" min="0"  id="accept_quality_rate" name="accept_quality_rate" class="vfg_form_field" value="'.$accept_quality_rate.'">';
echo '<p><small>Please fill in the number of lead’s minimum quality rating. The rating must be a number between 0 and 100, where 10 would mean only the highest quality input is accepted and 0 would mean all input is accepted. Default is 6.</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="accept_role_account" class="accept_role_account">' . __( 'Accept role accounts', 'vfg' ) . ':</label></th>';
echo '<td>';
echo '<input type="checkbox" id="accept_role_account" name="accept_role_account" class="vfg_form_field" value="1"'; if ( $accept_role_account == 1 ) echo ' checked="checked"'; echo '> accept role account.';
echo '<p><small>Check this option to accept role based email accounts such as info@domain.com.</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="main_error" class="main_error">' . __( 'Form Error Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="main_error" name="main_error" class="vfg_form_field" value="'.$main_error.'" >';
echo '<p><small>Please fill in the name of element which you want to display form warning or form error message (optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th style="color:green;"><label for="main_success_msg" class="main_success_msg">' . __( 'Form Success Message:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="main_success_msg" name="main_success_msg" class="vfg_form_field" value="'.$main_success_msg.'" style="width:100%;">';
echo '<p><small>Please fill in message which you want to display form warning or form success message (optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th style="color:red;"><label for="main_error_msg" class="main_error_msg">' . __( 'Form Error Message:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="main_error_msg" name="main_error_msg" class="vfg_form_field" value="'.$main_error_msg.'" style="width:100%;">';
echo '<p><small>Please fill in message which you want to display form warning or form error message (optional).</small></p>';
echo '</td>';
echo '</tr>';
echo '<tr><td colspan="2"><hr></td></tr>';

echo '<tr>';
echo '<th><label for="first_name" class="first_name_label">' . __( 'First Name Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="first_name" name="first_name" class="first_name_field" value="'.$first_name.'">';
echo '<p><small>Please fill in the name of first name field which you want to validate.</small></p>';
echo '</td>';
echo '</tr>';

//Error First Name
echo '<tr>';
echo '<th><label for="firstname_error" class="firstname_error_label">' . __( 'Error First Name Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="firstname_error" name="firstname_error" class="firstname_error_field" value="'.$firstname_error.'" >';
echo '<p><small>Please fill in the name of element which you want to display first name warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="firstname_error_msg">' . __( 'First Name Error Message:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="firstname_error_msg" name="firstname_error_msg" class="firstname_error_field" value="'.$firstname_error_msg.'" style="width:100%;">';
echo '<p><small>Please fill in the message which you want to display first name warning or error message (optional).</small></p>';
echo '</td>';
echo '</tr>';
echo '<tr><td colspan="2"><hr></td></tr>';

echo '<tr>';
echo '<th><label for="last_name" class="last_name_label">' . __( 'Last Name Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="last_name" name="last_name" class="last_name_field" value="'.$last_name.'">';
echo '<p><small>Please fill in the name of last name field which you want to validate.</small></p>';
echo '</td>';
echo '</tr>';

//Error Last Name
echo '<tr>';
echo '<th><label for="lastname_error" class="lastname_error_label">' . __( 'Error Last Name Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="lastname_error" name="lastname_error" class="lastname_error_field" value="'.$lastname_error.'" >';
echo '<p><small>Please fill in the name of element which you want to display last name warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="lastname_error_msg" >' . __( 'Last Name Error Message:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="lastname_error_msg" name="lastname_error_msg" class="lastname_error_field" value="'.$lastname_error_msg.'" style="width:100%;">';
echo '<p><small>Please fill in the text which you want to display last name warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';
echo '<tr><td colspan="2"><hr></td></tr>';

//Full Name
echo '<tr>';
echo '<th><label for="full_name" class="last_name_label">' . __( 'Full Name Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="full_name" name="full_name" class="full_name_field" value="'.$full_name.'">';
echo '<p><small>Please fill in the name of full name field which you want to validate.</small></p>';
echo '</td>';
echo '</tr>';

//Error Full Name
echo '<tr>';
echo '<th><label for="fullname_error" class="fullname_error_label">' . __( 'Error Full Name Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="fullname_error" name="fullname_error" class="fullname_error_field" value="'.$fullname_error.'" >';
echo '<p><small>Please fill in the name of element which you want to display full name warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="fullname_error_msg" class="fullname_error_msg_label">' . __( 'Full Name Error Message:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="fullname_error_msg" name="fullname_error_msg" class="fullname_error_field" value="'.$fullname_error_msg.'" >';
echo '<p><small>Please fill in the text which you want to display full name warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';
echo '<tr><td colspan="2"><hr></td></tr>';

echo '<tr>';
echo '<th><label for="email" class="email_label">' . __( 'E-Mail Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="email" name="email" class="email_field" value="'.$email.'">';
echo '<p><small>Please fill in the name of E-mail field which you want to validate.</small></p>';
echo '</td>';
echo '</tr>';

//Error E-Mail 
echo '<tr>';
echo '<th><label for="email_error" class="email_error_label">' . __( 'Error E-Mail Field:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="email_error" name="email_error" class="email_error_field" value="'.$email_error.'" >';
echo '<p><small>Please fill in the name of element which you want to display email address warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th><label for="email_error_msg" class="email_error_msg_label">' . __( 'E-Mail Error Message:', 'vfg' ) . '</label></th>';
echo '<td>';
echo '<input type="text" id="email_error_msg" name="email_error_msg" class="email_error_msg_field" value="'.$email_error_msg.'" >';
echo '<p><small>Please fill in the text which you want to display email address warning or error message (Optional).</small></p>';
echo '</td>';
echo '</tr>';
echo '</table>';
}

	
public function save_metabox( $post_id, $post ) {

// Add nonce for security and authentication.	
if ( isset( $_POST['vfg_form_nonce']) ){
			
$nonce_name   = $_POST['vfg_form_nonce'];		
$nonce_action = 'vfg_form_nonce_action';
		
// Check if a nonce is set.		
if ( ! isset( $nonce_name ) )			
return;
		
// Check if a nonce is valid.		
if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )			
return;
		
// Check if the user has permissions to save data.		
if ( ! current_user_can( 'edit_post', $post_id ) )			
return;
		
// Check if it's not an autosave.		
if ( wp_is_post_autosave( $post_id ) )				
return;
		
// Check if it's not a revision.			
if ( wp_is_post_revision( $post_id ) )			
return;
		
// Sanitize user input			
$vfg_form_name = isset( $_POST[ 'vfg_form_name' ] ) ? sanitize_text_field( $_POST[ 'vfg_form_name' ] ) : '';
$vfg_form_ajax = isset( $_POST[ 'vfg_form_ajax' ] ) ? sanitize_text_field( $_POST[ 'vfg_form_ajax' ] ) : 0;
$vfg_form_redirect = isset( $_POST[ 'vfg_form_redirect' ] ) ? sanitize_text_field( $_POST[ 'vfg_form_redirect' ] ) : "";
$accept_quality_rate = isset( $_POST[ 'accept_quality_rate' ] ) ? sanitize_text_field( $_POST[ 'accept_quality_rate' ] ) : 5;
$accept_role_account = isset( $_POST[ 'accept_role_account' ] ) ? sanitize_text_field( $_POST[ 'accept_role_account' ] ) : 0;
$main_error = isset( $_POST[ 'main_error' ] ) ? sanitize_text_field( $_POST[ 'main_error' ] ) : '';
$main_success_msg = isset( $_POST[ 'main_success_msg' ] ) ? sanitize_text_field( $_POST[ 'main_success_msg' ] ) : '';
$main_error_msg = isset( $_POST[ 'main_error_msg' ] ) ? sanitize_text_field( $_POST[ 'main_error_msg' ] ) : '';
$first_name = isset( $_POST[ 'first_name' ] ) ? sanitize_text_field( $_POST[ 'first_name' ] ) : '';
$last_name = isset( $_POST[ 'last_name' ] ) ? sanitize_text_field( $_POST[ 'last_name' ] ) : '';
$full_name = isset( $_POST[ 'full_name' ] ) ? sanitize_text_field( $_POST[ 'full_name' ] ) : '';
$email = isset( $_POST[ 'email' ] ) ? sanitize_text_field( $_POST[ 'email' ] ) : '';
$firstname_error = isset( $_POST[ 'firstname_error' ] ) ? sanitize_text_field( $_POST[ 'firstname_error' ] ) : '';
$firstname_error_msg = isset( $_POST[ 'firstname_error_msg' ] ) ? sanitize_text_field( $_POST[ 'firstname_error_msg' ] ) : '';
$lastname_error = isset( $_POST[ 'lastname_error' ] ) ? sanitize_text_field( $_POST[ 'lastname_error' ] ) : '';
$lastname_error_msg = isset( $_POST[ 'lastname_error_msg' ] ) ? sanitize_text_field( $_POST[ 'lastname_error_msg' ] ) : '';
$fullname_error = isset( $_POST[ 'fullname_error' ] ) ? sanitize_text_field( $_POST[ 'fullname_error' ] ) : '';
$fullname_error_msg = isset( $_POST[ 'fullname_error_msg' ] ) ? sanitize_text_field( $_POST[ 'fullname_error_msg' ] ) : '';
$email_error = isset( $_POST[ 'email_error' ] ) ? sanitize_text_field( $_POST[ 'email_error' ] ) : '';
$email_error_msg = isset( $_POST[ 'email_error_msg' ] ) ? sanitize_text_field( $_POST[ 'email_error_msg' ] ) : '';

			
// Update the meta field in the database.
			
update_post_meta( $post_id, 'vfg_form_name', $vfg_form_name );
update_post_meta( $post_id, 'vfg_form_ajax', $vfg_form_ajax );
update_post_meta( $post_id, 'vfg_form_redirect', $vfg_form_redirect );
update_post_meta( $post_id, 'accept_quality_rate', $accept_quality_rate );
update_post_meta( $post_id, 'accept_role_account', $accept_role_account );
update_post_meta( $post_id, 'main_error', $main_error );
update_post_meta( $post_id, 'main_success_msg', $main_success_msg );
update_post_meta( $post_id, 'main_error_msg', $main_error_msg );
update_post_meta( $post_id, 'first_name', $first_name );	
update_post_meta( $post_id, 'last_name', $last_name );
update_post_meta( $post_id, 'full_name', $full_name );
update_post_meta( $post_id, 'email', $email );	
update_post_meta( $post_id, 'firstname_error', $firstname_error );
update_post_meta( $post_id, 'firstname_error_msg', $firstname_error_msg );	
update_post_meta( $post_id, 'lastname_error', $lastname_error );
update_post_meta( $post_id, 'lastname_error_msg', $lastname_error_msg );	
update_post_meta( $post_id, 'fullname_error', $fullname_error );
update_post_meta( $post_id, 'fullname_error_msg', $fullname_error_msg );	
update_post_meta( $post_id, 'email_error', $email_error );	
update_post_meta( $post_id, 'email_error_msg', $email_error_msg );	
}
		
}

}
$qwe = new vfg_form_Meta_Box;

function Setting_layout(){
	include("include/settings.php");
}

function Verifigator_layout(){
?>
	<div class="wrap">
		<h2>Welcome To Verifigator Menu</h2>		
	</div>
	<?php

}

function Overview_layout() {
?>
	
	<div class="wrap">
		<h2>Welcome To Overview Menu</h2>
	</div>
	<?php

}

/*
*** Generate the JS Script and Enqueue to header
*/
function verifigator_head_script(){
	include(plugin_dir_path(__FILE__)."include/script.php");
	create_js_scripts();
}
add_action( 'wp_head', 'verifigator_head_script' );

