<?php

 /*

    This is a PHP class for using the Verifigator API.

    The Verifigator API allows you to validate user input data
    such as an email address and/or name

    To be able to use the API, you must first get an API Key.
    You can get a free API Key from: https://www.verifigator.com

    For the API documentation, please see: https://verifigator.com/api-docs/

    Version: (see code below)
    Author: Arkham Enterprises
    Author URI: https://www.verifigator.com/
    License: GPLv2 or later

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

    Copyright 2017-2018 Arkham Enterprises, Inc.



    CHANGE LOG
    ==

    1.0.2.3 - New feature: lead_export() now supports the $limit input parameter.
              New function: help() returns the API's help data and endpoint listing.
              Removed function endpoints().

    1.0.2.2 - New function: changelog() and changelog_api() to retrieve the change logs from the server.
    1.0.2.1 - New function: lead_import_delete() and lead_export_delete() to abort an ongoing lead import or export operation
    1.0.2.0 - New feature: Leads pagination via the $limit input parameter of lead_search() function

    ==


 */


class VerifigatorIntegrator
{
    // Version of this class:
    public $api_integrator_version = '1.0.2.3';

    // Version of the API server (this will be set after any API call)
    public $api_server_version = '';


    // Verifigator API base URL:
    private $api_url = 'https://api.verifigator.com/v1/';

    // The use of the Verifigator API requires an API key.
    // You can get one for free from verifigator.com
    private $api_key;

    // Read this array to get debug data of the last API call
    public $debug_data = array();

    // If enabled, we are asking the API server to return a formatted JSON response
    // You can enable this if you are doing debugging and print results to screen
    public $pretty_print = false;

    // If enabled, the code will call die() to kill the execution in case of any
    // API call returns an error message. Can become handy for debugging -
    // However, do notice that sometimes an error message can be an expected outcome
    // of an API call and not a sign of a failure, for example
    // calling status_import(0) to get the status of the last import
    // opearation will return error code 404 in case no imports found.
    public $die_on_fail = false;

    // Only for internal use. Do not use.
    public $custom_data_arr = array();

    // Stores all the made API calls for testing and debugging purposes
    public $api_log_arr = array();

    // The number of API calls remaining. Private member, do not attempt to read.
    // To get the number of API calls remaining, please call get_remaining_api_calls()
    private $api_calls_remaining = 0;
    private $api_calls_last_checked = 0;

    // Contains the raw API response of the last API call as an array. Contains an empty array() in case no API call made yet.
    public $last_api_response = array();


    // $api_key must be provided for using the class for anything else than calling apikey_validate()
    // $callerMessage is an optional parameter that allows you to add a message for your internal
    // use for your API keys. For example, it can be used to separate API calls originating from
    // different areas of your website or application
    public function __construct($api_key = '')
    {
        $this->api_key = $api_key;
        $this->api_log_arr = array();

        // Ensure we have CURL installed:
        if (!function_exists('curl_init') or
            !function_exists('curl_setopt') or
            !function_exists('curl_exec'))
        {
            throw new Exception("Fatal Error: curl not installed! VerifigatorIntegrator requires curl.");
        }

        // Read stored $api_calls_remaining data
        if (!empty($this->api_key) and
            isset($_SESSION['verifigator_api_calls_remaining']) and
            isset($_SESSION['verifigator_api_calls_remaining'][$this->api_key]) and
            isset($_SESSION['verifigator_api_calls_remaining'][$this->api_key]['calls']) and
            isset($_SESSION['verifigator_api_calls_remaining'][$this->api_key]['checked']))
        {
            $this->api_calls_remaining = intval($_SESSION['verifigator_api_calls_remaining'][$this->api_key]['calls']);
            $this->api_calls_last_checked = floatval($_SESSION['verifigator_api_calls_remaining'][$this->api_key]['checked']);
        }
    }

    // Returns the API key currently being used
    public function get_api_key()
    {
        return $this->api_key;
    }

    // Finds the JSON response from the HTTP response and decodes it as an array.
    // This is used in case of an internal server error causes error messages or other
    // unrelating data to be included in the server response. It should not happen,
    // but we are assuming things that cannot happen can happen. Thusly:
    private function process_response($response)
    {
        $result_arr = array();

        // Ensure we are getting only a pure JSON response:
        if (is_string($response) and strpos($response, '{') !== false and strpos($response, '}') !== false)
        {
            $idx = strpos($response, '{');
            if ($idx > 0) $response = substr($response, $idx);

            $idx = strrpos($response, '}');
            if ($idx !== false) $response = substr($response, 0, $idx+1);

            $result_arr = @json_decode($response, true);

        }

        if (!is_array($result_arr) or empty($result_arr))
        {
            $result_arr = array(
                'success'            => 0,
                'error_code'         => 500,
                'error_message'      => 'Invalid server response - this response generated by class.VerifigatorIntegrator ('.$this->api_integrator_version.')',
                'api_server_version' => $this->api_server_version,
                'server_response'    => print_r(strip_tags($response), true)
            );
        }


        return $result_arr;
    }

    // Generates the final data array to be sent to the API server
    private function generate_payload($data_arr)
    {
        if (!is_array($data_arr)) $data_arr = array();

        if ($this->pretty_print) $data_arr['pretty'] = 1;
        $data_arr['caller_plugin']   = 'VI:PHP:' . $this->api_integrator_version;
        $data_arr['caller_server']   = $_SERVER['SERVER_NAME'];
        $data_arr['caller_host']     = $_SERVER['HTTP_HOST'];
        $data_arr['caller_ip']       = $_SERVER['REMOTE_ADDR'];

        if (empty($data_arr['api_key']))
            $data_arr['api_key'] = $this->api_key;

        if (!empty($this->custom_data_arr))
        {
            foreach ($this->custom_data_arr as $key => $value)
                    $data_arr[$key] = $value;
        }


        // In the case the input data looks tricky, let's base64 encode it just in case:
        foreach ($data_arr as $key => $val)
        {
            if (preg_match('/[^a-z0-9\.\-\:\_]/i', $val))
            {
                $val = 'base64:'. base64_encode($val);
                $data_arr[$key] = $val;
            }
        }



        $this->debug_data['last_payload'] = $data_arr;

        return $data_arr;
    }

    // Sends a HTTP POST to the Verifigator API url and returns the API response array
    // Usage: to create or add data on server
    public function http_post($end_point, $data_arr)
    {
        return $this->do_curl($end_point, $data_arr, 'POST');
    }

    // Sends a HTTP PUT to the Verifigator API url and returns the API response array
    // Usage: to update data on server
    public function http_put($end_point, $data_arr)
    {
        return $this->do_curl($end_point, $data_arr, 'PUT');
    }

    // Sends a HTTP GET to the Verifigator API url and returns the API response array
    // Usage: to get data from server
    public function http_get($end_point, $data_arr)
    {
        return $this->do_curl($end_point, $data_arr, 'GET');
    }

    // Sends a HTTP DELETE to the Verifigator API url and returns the API response array
    // Usage: to delete data from server
    public function http_delete($end_point, $data_arr)
    {
        return $this->do_curl($end_point, $data_arr, 'DELETE');
    }

    // Performs the actual HTTP call using curl
    private function do_curl($end_point, $data_arr, $method)
    {
        $cur_api_url = $this->api_url . trim($end_point, ' /') . '/';
        $api_call_arr = array();

        $post_data_arr = $this->generate_payload($data_arr);
        $post_data_arr['method']   = $method;
        $post_data_arr['endpoint'] = $end_point;
        $ch = curl_init();

        $api_call_arr['method'] = $method;
        $api_call_arr['data']   = $post_data_arr;

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data_arr));
        $this->debug_data['last_postdata'] = $post_data_arr;


        $this->debug_data['last_url']       = $cur_api_url;
        $this->debug_data['last_method']    = $method;
        $this->debug_data['last_endpoint']  = $end_point;


        // Perform the HTTP POST/GET/PUT/DELETE using curl:
        curl_setopt($ch, CURLOPT_URL            , $cur_api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION , 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST  , $method);


        $start = microtime(true);
        $response_arr = $this->process_response(curl_exec($ch));
        $response_time = microtime(true)-$start;

        $api_call_arr['response_array'] = $response_arr;

        if (is_array($response_arr) and isset($response_arr['response_time']))
             $api_call_arr['response_time_reported'] = $response_arr['response_time'];
        else $api_call_arr['response_time_reported'] = -1;
        $api_call_arr['response_time_measured'] = $response_time;

        $this->api_log_arr[] = $api_call_arr;

        $this->debug_data['last_curl_info']     = curl_getinfo($ch);
        $this->debug_data['last_curl_error']    = curl_error($ch);
        curl_close($ch);

        $this->debug_data['last_response'] = $response_arr;

        // Update the api_calls_remainig if possible:
        if (isset($response_arr['api_calls_remaining']))
        {
            $this->api_calls_remaining = intval($response_arr['api_calls_remaining']);
            $this->api_calls_last_checked = microtime(true);

            // Also store the $api_calls_remaining data to $_SESSION
            if ($this->is_session_started())
            {
                $_SESSION['verifigator_api_calls_remaining'][$this->api_key]['calls'] = $this->api_calls_remaining;
                $_SESSION['verifigator_api_calls_remaining'][$this->api_key]['checked'] = $this->api_calls_last_checked;
            }
        }

        // Set the $this->api_server_version if empty:
        if (empty($this->api_server_version) and isset($response_arr['api_version']))
        {
            $this->api_server_version = $response_arr['api_version'] . '/' . $response_arr['api_server'];
            if ($this->is_session_started()) $_SESSION['verifigator_api_server_version'] = $this->api_server_version;
        }

        if ($this->die_on_fail)
        {
            if (!isset($response_arr['success']) or
                $response_arr['success'] != 1)
            {
              echo '<h1 style="color:red;">Fatal Error</h1><br>Verifigator API call failed!<br>';
              echo 'Debug data:<pre>'; print_r($this->debug_data); echo '</pre><br><br>';
              echo 'You see this message because VerifigatorIntegrator class die_on_fail = true. Calling die(); now.';
              die();
            }
        }

        $this->last_api_response = $response_arr;
        return $response_arr;
    }


    // Returns a status array from the server, containing information such as server version
    public function status()
    {
        $res = $this->http_get('status', null);
        return $res;
    }


    // Returns true if PHP $_SESSION has been started:
    private function is_session_started()
    {
        if (php_sapi_name() !== 'cli' ) {
            if ( version_compare(phpversion(), '5.4.0', '>=') ) {
                return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
            } else {
                return session_id() === '' ? FALSE : TRUE;
            }
        }

        return FALSE;
    }
    // Returns information of the given api key
    public function status_apikey($api_key)
    {
        $res = $this->http_get('status/apikey', array('api_key' => $api_key));
        return $res;
    }

    // Returns an array of the possible lead export format and columns options
    // These are matching the $format and $columns_arr input parameters of lead_export
    public function lead_export_options()
    {
        $res = $this->http_get('status/export', null);
        return $res;
    }



    /*
        Starts a lead export operation

        To read the status of a started lead export operation, please use status_export()

        To start a lead export operation using an existing integration method (e.g. remote MySQL connection or MailChimp),
        please provide the name of the integration method as the $format parameter,
        for example let's say you have created an integration method with
            integration_create('Remote database', 'mysql', 'export',  array(...));
        you can then use lead_export('', null, '<Remote database>'); to use the integration method
        to export the data, to a remote database in this case.

        If $quality_rating is set, only leads matching with the given $quality_rating value are returned.
        $quality_rating supports following operands: <, >, >=, <= and =.
        For example, calling lead_export('', null, 'csv', null, '10') will export all leads which have quality rating of 10.
        calling lead_export('', null, 'csv', null, '>5') will export all leads which have quality rating of 6 or higher.
        calling lead_export('', null, 'csv', null, '<=4') will export all leads which have quality rating of 4 or lower.

        Setting the $limit parameter allows you to limit the maximum number of exported leads.
        Calling lead_export('', null, 'csv', null, null, 10); will export a maximum of 10 leads.
        Calling lead_export('', null, 'csv', null, null, '10, 15'); will export a maximum of 10 leads, starting from lead number 15.
        Thus, the $limit parameter works identical to MySQL's LIMIT clause.
    */
    public function lead_export($search_phrase = '', $tags_arr = null, $format = 'csv', $columns_arr = array(), $quality_rating = null, $limit = null)
    {
        $columns = implode(',', $columns_arr);
        if (is_array($tags_arr)) $tags_arr = serialize($tags_arr); else $tags_arr = null;

        $res = $this->http_get('leads/export',
                            array('search'    => $search_phrase,
                                  'tags'      => $tags_arr,
                                  'format'    => $format,
                                  'columns'   => $columns,
                                  'quality'   => $quality_rating,
                                  'limit'     => $limit));

        return $res;
    }

    // Aborts / cancels an ongoing export operation.
    // If the export operation has already been completed, this call does nothing.
    // If export operation is not found, an error code of 404 is returned
    public function lead_export_delete($export_id)
    {
        $res = $this->http_delete('leads/export/' . $export_id, null);
        return $res;
    }

    // Returns an information array of the current status of the given export operation (by $export_id)
    public function status_export($export_id)
    {
        $res = $this->http_get('status/export/' . $export_id, null);

        return $res;
    }

    // Delete a lead from the lead list.
    // Note: $lead_id can be one lead_id, or array of many lead_ids.
    public function lead_delete($lead_id)
    {
        if (is_array($lead_id)) $lead_id = implode(',', $lead_id);

        $res = $this->http_delete('leads/'. $lead_id, null);

        return $res;
    }

    // Deletes all leads from the user's lead list
    public function lead_delete_all()
    {
        $res = $this->http_delete('leads/all', null);

        return $res;
    }

    // Performs a validation on the given lead
    // and returns the full validation array
    // Input: user's email address, user's full name (optional but recommended)
    // Output: array
    // API call: GET/POST leads/validate
    public function lead_validate($email, $name, $add_to_list = false)
    {
        $data_arr = array(
           'email_address'  => $email,
           'full_name'      => $name
        );

        if (!$add_to_list) $result = $this->http_get('leads/validate', $data_arr);
        else $result = $this->http_post('leads/validate', $data_arr);

        return $result;
    }

    // Performs a validation on the given lead
    // and returns either 'valid' in case of a lead
    // not matching the given criteria, a reason why it is not matching
    // Input: user's email address, user's full name (optional but recommended)
    // Output: string, 'valid' in case a valid input and the error code for invalid input.
    public function lead_validate_simple($email, $name,
        $minimum_quality        = 40,
        $allow_partial_name     = true,
        $allow_corporate_name   = true,
        $allow_role_based       = true,
        $allow_free_email       = true,
        $allow_spamtrap         = false,
        $allow_disposable       = false,
        $allow_geo_locations    = array('*'),
        $allow_languages        = array('*'),
        $allow_genders          = array('M', 'F', 'N')
        )
    {

        $result = '';
        $arr = $this->lead_validate($email, $name);


        if (is_array($arr) and isset($arr['error_code']) and $arr['error_code'] > 0)
        {
            $result = 'api error: ' . $arr['error_code'] . ' ('.$arr['error_message'].')';
        }

        if (empty($result))
        {
            if (is_array($arr) and isset($arr['success']) and $arr['success'] > 0)
            {
                if ($result == '' and $arr['valid_email'] != 1) $result = 'invalid_email';

                if ($result == '' and $arr['quality_rating'] < $minimum_quality) $result = 'quality_rating';

                // $allow_partial_name defines whether we should accept a lead with only a single name
                // such as only a first name or last name.
                // E.g. "John", "Mr. John", or "Dr. Smith, Sr." => partial name
                // "John Smith" or "Mr. John Smith" => full name (not partial name)
                if ($result == '' and !$allow_partial_name and
                    (empty($arr['user_firstname']) or empty($arr['user_lastname']))) $result = 'partial_name';

                // $allow_corporate_name defines whether we should accept a company name as the lead name
                // E.g. "Microsoft Corporation" => corporate name
                // "John Smith" => personal name (not corporate name)
                if ($result == '' and !$allow_corporate_name and $arr['user_corporatename'] == 1) $result = 'corporate_name';

                // $allow_role_based defines whether we should accept role based email accounts
                // E.g. "info@domain.com", "sales@domain.com", 'newyork.office@domain.com" => role based
                // "john.smith@domain.com", "johnsemail@domain.com" => personal email (not role based)
                if ($result == '' and !$allow_role_based and $arr['role_account'] == 1) $result = 'role_account';

                // $allow_free_email defines whether we should accept free email addresses
                // E.g. "john.smith@gmail.com" and "john.smith@yahoo.com" => free email address
                // "john.smith@microsoft.com" => not free email address
                if ($result == '' and !$allow_free_email and $arr['free_account'] == 1) $result = 'free_account';

                // $allow_spamtrap defines whether we should accept so called spamtrap email addresses,
                // i.e. automatically generated email addresses which are used to catch spammers
                // normally there is no reason to accept this kind of email addresses
                if ($result == '' and !$allow_spamtrap and $arr['spamtrap'] == 1) $result = 'spamtrap';

                // $allow_disposable defines whether we should accept so called disposable email addresses
                // also known as one time use email addresses or throw-away email addresses, which are
                // email addresses that are generated to be used only once for example to enter
                // when forced to register to a service. Normally there is no need to accept these.
                if ($result == '' and !$allow_disposable and $arr['disposable'] == 1) $result = 'disposable';



                // $allow_geo_locations defines which countries we are accepting users from
                if ($result == '' and is_array($allow_geo_locations) and implode('', $allow_geo_locations) != '*')
                {
                    $geo_threshold = 40;
                    $geo_arr = $arr['geo_location_array'];

                    //API returns the country codes in UPPER CASE, thusly ensure the input is also uppercase
                    foreach ($allow_geo_locations as $key => $val)
                        $allow_geo_locations[$key] = trim(strtoupper($val));

                    $found = false;
                    foreach ($geo_arr as $country_code => $probability)
                    {
                        if ($probability < $geo_threshold) break;
                        if (in_array($country_code, $allow_geo_locations))
                        {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) $result = 'geo_location';
                }


                // $allow_languages defines which language speaking users we are accepting
                if ($result == '' and is_array($allow_languages) and implode('', $allow_languages) != '*')
                {
                    $lang_threshold = 40;
                    $lang_arr = $arr['language_array'];

                    //API returns the country codes in lowercase, thusly ensure the input is also lowercase
                    foreach ($allow_languages as $key => $val)
                        $allow_languages[$key] = trim(strtolower($val));

                    $found = false;
                    foreach ($lang_arr as $lang_code => $probability)
                    {
                        if ($probability < $lang_threshold) break;
                        if (in_array($lang_code, $allow_languages))
                        {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) $result = 'language';
                }

                // $allow_genders defines of which gender users we are accepting.
                // Notice: gender option 'N' is used for cases of corporate users and for
                // users whose gender cannot be detected from the name and email address data.
                if ($result == '' and is_array($allow_genders) and implode('', $allow_genders) != 'MFN')
                {
                    $user_gender = $arr['gender'];
                    if (!in_array($user_gender, $allow_genders))  $result = 'gender';
                }


                if ($result == '') $result = 'valid';

            } else $result = 'error: invalid api response.';
        }

        return $result;
    }




/*
    ***
    Functions relating to the List Manager.
    **

    Verifigator allows one user to have one lead list.
    One lead list can contain 0 .. unlimited number of 'leads'.
    One lead means information of one person, at minimum, an email address of that person.

    A lead can have the following information:
    *) ID number (lead_id) - a globally unique ID number for this lead,
       used only internally by the system, not shown in the user interface.
    *) Email address (required)
    *) First name
    *) Last name
    *) Gender
    *) Country
    *) Language
    *) 0 .. unlimited number of tags.

    A tag is the main method how Verifigator allows users to manage their lead lists, and
    to allow users to have more than one lead lists (e.g. by tagging some leads with "list one" and other with "list two" for example)

    A tag consists of
    *) ID number (tag_id) - a globally unique ID number for this tag,
       used only internally by the system, not shown in the user interface
    *) Name (required, cannot be numeric)

    User can create and delete tags. A tag could be for example "Customer" or "Potential customer" or
    "Old customer".

    Note: Tag names are unique. User cannot create two tags with the same name.
*/



/*
        Adds the given lead(s) to user's lead list OR updates the data of given lead(s)

        Import data is the data submitted by user, either a string that contains
        one lead (name + email), or
        multiple leads in format of one lead per line, or
        an URL of file (list of leads) to download.

        In case $tags_arr is provided, the tag(s) listed in $tags_arr are added to each
        imported lead.

        For example,
        calling
            lead_import('<John Smith> john.smith@gmail.com') will
        add the lead 'John Smith' to user's lead list. In case lead with the same
        email address already exists, nothing is added to the list.

        Calling
            lead_import('<John Smith> john.smith@gmail.com', array('Customer' => true)) will
        add the lead 'John Smith' to user's lead list and assign tag 'Customer' to this lead.
        In case the lead already exists but does not have the 'Customer' tag, the existing
        lead is updated by adding the 'Customer' tag to the lead.

        Calling
                lead_import("<John Smith> john.smith@gmail.com\n<John Doe> jd@domain.com",
                        array('Customer' => true, 'From Canada' => true, 'spam' => false))

        will add the leads 'John Smith' and 'John Doe' to user's lead list and
        assign tags 'Customer' and 'From Canada' to both leads. In case either lead already
        exists in the list, their data is updated. If either lead exists and has a tag 'spam',
        the 'spam' tag is removed from the lead(s).

        Calling
                lead_import("https://yourdomain.com/leads.csv")

        will make the API Server to download the leads.csv file and analyze it to find leads,
        and add all found leads to user's lead list.


        Note: To use a previously created integration method (e.g. remote database or MailChimp)
        as the source of the imported data, simply call:
        lead_import('<integration method name>');
        For example, if you have used integration_create() to create a remote database integration
        with the name of 'my remote database', you can use that to import its data to Verifigator
        by calling:
        lead_import('<my remote database>');


        NOTE: The import of data can have a delay!
        To check when the import operation has been completed,
        first run list_import(), record the import_id variable from its return array and use that
        import_id to call status_import()
*/

    public function lead_import($import_data, $tags_arr = null)
    {
        if (is_array($import_data)) $import_data = implode("\n", $import_data);
        if (is_array($tags_arr)) $tags_arr = serialize($tags_arr); else $tags_arr = null;

        $res = $this->http_post('leads',
                            array('data'    => $import_data,
                                  'tags'    => $tags_arr));

        return $res;
    }


    // Aborts / cancels an ongoing import operation.
    // If the import operation has already been completed, this call does nothing.
    // If import operation is not found, an error code of 404 is returned
    public function lead_import_delete($import_id)
    {
        $res = $this->http_delete('leads/import/' . $import_id, null);
        return $res;
    }


    /*
        Creates a new integration method. An integration method allows user to
        make Verifigator to communicate with third party systems, for example to connect
        to your remote MySQL server or talk with your Mailchimp.

        For the list of currently supported integration methods, please see: https://verifigator.com/api-docs/

        The input parameters are:
            $name: A user defined name of this integration method
            $type: Keyword which defines the type of integration, for example 'adodb:import' to define a remote database connection to import data to Verifigator (uses the adodb library)
            $data_arr: an array containing the details relating to this integration type and mode.

        For example, calling
            integration_create('Remote MySQL', 'db:import',
                array('dsn' => 'mysqli://username:password@remotehost.com/databasename',
                      'sql' => 'SELECT lead_name,lead_email FROM my_leads WHERE 1'));

        Would create a remote MySQL integration which can be then used to import data to Verifigator
        by calling lead_import('[Remote MySQL]');
        The remote database connection uses the ADOdb library. For more information how to connect to
        other remote databases than MySQL, please see: http://adodb.org/dokuwiki/doku.php?id=v5:reference:connection:adonewconnection


        If you do not wish Verifigator to store your password, you can leave the password blank when
        creating the integration and calling as:
        lead_import('[Remote MySQL:mysecretpassword123]');

        Note: Calling integration_create() with the same $name and $type will overwrite
        any previously created integration methods. This can be used to edit the $data_arr of a previously
        create integration, for example to change the stored password.

        Thus, calling integration_create('Remote database', 'db:import',  array(...));
        and then calling integration_create('Remote database', 'db:import',  new_array(...));
        will overwrite the old $data_arr with the new_array.
        But calling integration_create('Remote database', 'db:export', new_array(...));
        would create a new integration method for exporting data from Verifigator to a remote database

    */

    public function integration_create($name, $type, $data_arr)
    {
        $data = 'base64:' . base64_encode(serialize($data_arr));

        $res = $this->http_post('integration',
            array('name'    => $name,
                  'type'    => $type,
                  'data'    => $data));

        return $res;
    }

    /*
        Returns a list of all created integration methods

        If $name and/or $type is provided, only the matching integration method(s) is/are returned.

        NOTE: This function never returns a saved integration method password or secret API key,
        instead a value with *** characters is returned. The number of * characters indicate the length
        of the stored password or secret API key.
    */

    public function integration_get($name = '', $type = '')
    {
        $res = $this->http_get('integration',
                        array('name'    => $name,
                              'type'    => $type));

        return $res;
    }

    // Deletes the given integration method
    public function integration_delete($name, $type)
    {
        $res = $this->http_delete('integration',
                        array('name'    => $name,
                              'type'    => $type));

        return $res;
    }


    /*
        Edits the given field of an integration method.

        The possible values for $update_field are "name" and all the data fields of the integration method.
        Note: It is not possible to use the edit feature to change the type of an integration method.

        For example:
            integration_edit('my integration', 'url:import', 'name', 'new integration name')

        As integrations of type 'url:import' have a required parameter of 'url', that can also be edited:
        integration_edit('my integration', 'url:import', 'url', 'https://domain.com/leads_file.txt')

        Note: There is minimal input data validation. For example, you can enter an invalid url as the input for
        the 'url' field. It is your responsibility to ensure the input values are valid.

        Note: If you edit the name of an integration method and an integration method with the same name already
        exists, the API call will return error code 409.
    */

    public function integration_edit($name, $type, $update_field, $new_value)
    {
        $result = $this->http_put('integration',
                                array('name'         => $name,
                                      'type'         => $type,
                                      'update_field' => $update_field,
                                      'new_value'    => $new_value));

        return $result;
    }


    // Returns the syntax declarations of all of the currently supported integration methods
    public function integration_syntax()
    {
        $result = $this->http_get('syntax/integration', array());
        return $result;
    }

    /*

     Performs a test to see if the given integration method works
     The test is done in a way it does not alter any data in user's system,
     for example no leads are imported or exported.

     The nature of the exact set of tests depends on the integration method type,
     for example in a case of 'remote database' type integration,
     the test would check the remote database connection details look valid (syntax check)
     and attempt to establish the remote database connection without actually fetching any data.

    */

    public function integration_test($name, $type)
    {
        $result = $this->http_get('integration/test',
                          array('name'    => $name,
                                'type'    => $type));
        return $result;
    }


    // Executes the given integration.
    public function integration_run($name, $type)
    {
        $result = $this->http_get('integration/test',
                                   array('name'    => $name,
                                         'type'    => $type));
        return $result;
    }


    // Returns information of the last integration methods ran, either
    // automatically by the system or manually by the user.
    public function status_integration()
    {
        $result = $this->http_get('status/integration', null);
        return $result;
    }

    // Returns an information array of the current status of the given import operation (by $import_id)
    public function status_import($import_id)
    {
        $result = $this->http_get('status/import/' . $import_id, null);

        return $result;
    }


    // Returns the user's Verifigator settings
    public function settings_get()
    {
        $res = $this->http_get('settings/', null);
        return $res;
    }

    /*
     Sets the Verifigator settings for the user.
     To edit a setting, first get the latest settings data:

     $settings_array = settings_get();

     Then edit the $settings_array's content as you wish, and update to server:
     settings_set($settings_array);

     */
    public function settings_set($settings_array)
    {
        $settings_array = serialize($settings_array);
        $res = $this->http_put('settings/', array('settings' => $settings_array));

        return $res;
    }


    /*
    Updates a given field with a new value of a lead.

    Possible $update_field values are:
        "email", "firstname", "lastname", "name", "country", "language", "type"

    Note: A lead list can never contain duplicate leads, i.e. leads which have the same email address.
    If you call lead_edit() and try to update a lead's email field to a new value that already
    exists in your list, this function will return an error code 409.

    */

    public function lead_edit($lead_id, $update_field, $new_value)
    {
        if (is_array($lead_id)) $lead_id = implode(',', $lead_id);

        $res = $this->http_put('leads/' . $lead_id,
                            array('update_field' => $update_field,
                                  'new_value'    => $new_value));


        return $res;
    }

    /*

    Adds or removes tag(s) from a lead.

    In the case of the $tags_arr = array('Customer' => true) the tag 'Customer' is added to
    to the given lead.

    In the case of the $tags_arr = array('Good Lead' => false) the tag 'Good Lead' is removed
    from the given lead.

    */

    public function lead_edit_tags($lead_id, $tags_arr)
    {
        if (is_array($lead_id)) $lead_id = implode(',', $lead_id);
        if (is_array($tags_arr)) $tags_arr = serialize($tags_arr); else $tags_arr = null;

        $res = $this->http_put('leads/' . $lead_id,
                                array('update_field' => 'tags',
                                      'new_value'    => $tags_arr));

        return $res;
    }


    // Lists the most popular tags from the system. If $user_id is given, the listed
    // tags are the most popular tags used by this user, if no $user_id is given,
    // the result will be the most popular tags of all users.
    // Optional input parameter $limit defines the number of results desired.
    public function tags($user_id = null, $limit = 20)
    {
        if (is_null($user_id) or empty($user_id)) $user_id = '';
        $res = $this->http_get('tags/' . $user_id, array('limit' => $limit));
        return $res;
    }

    // Lists all the API endpoints from the API server with their description and other information.
    public function help()
    {
        $res = $this->http_get('help', null);
        return $res;
    }

    // Performs a simple connectivity test with the API back-end
    public function ping()
    {
        $res = $this->http_get('ping', null);
        return $res;
    }

    
    // Retrieves the summary of user's account data. If tags_arr is provided,
    // the data is returned only of the leads which are matching tags
    public function summary($tags_arr = null)
    {
        if (is_array($tags_arr)) $tags_arr = serialize($tags_arr); else $tags_arr = null;

        $res = $this->http_get('summary', array('tags'=> $tags_arr));
        return $res;
    }

    
    /*
        Get the full lead data of one lead OR many leads.
        If you want information of many leads at same time,
        pass an array as the $lead_id.

        For example lead_get(12345); will return the data of lead_id = 12345.

        For example lead_get( array(12345, 12346, 12347, 12399) );
        will return the data of the four given lead_id's.

        Protip: lead_get(0); will always return the lead data of the most recently added lead from your list.
    */

    public function lead_get($lead_id)
    {
        if (is_array($lead_id)) $lead_id = implode(',', $lead_id);

        $res = $this->http_get('leads/' . $lead_id, null);

        return $res;
    }

/*
        Returns an information array of the matching leads from user's lead list.

        In case $search_phrase is provided, the result contains information of list items
        matching the $search_phrase.

        If $tags_arr is provided, the result contains information of list items matching
        the $tags_arr. $tags_arr is recommended to be in format of array('Tag name' => true/false).

        For example, calling lead_search() will return all leads from user's lead list.

        Calling lead_search('foobar') will return leads of the user's lead list
        that match the given search word 'foobar'.

        Calling lead_search('foobar', array('Customer' => true, 'USA' => false)) will return
        the lead list items that match the given search word 'foobar' AND which HAVE the tag 'Customer'
        AND which DO NOT have tag 'USA',
        i.e. this search would return all leads who are customers outside of the US.

        In case $return_full_data = false, the function will only return the lead_id(s) of the
        matching lead(s) instead of the full data.

        Calling lead_search('', array('Customer', 'USA')) will return all leads which have both Customer and USA tag
        and will be identical to calling lead_search('', 'Customer, USA');

        All the searching is done in AND mode, meaning any search terms you enter must ALL match
        in order for a lead to be listed as a match. The only exception is if you give a $search_phrase
        which is a tag name, in which case the search returns all leads which either has this tag OR matches
        the search phrase. For example lead_search('male') will return all leads with the tag "Male" assigned to them,
        AND all leads whose name or email address contains the string "male".

        If $quality_rating is set, only leads matching with the given $quality_rating value are returned.
        For example, calling lead_search('', null, true, '10') will return all leads which have quality rating of 10.
        calling lead_search('', null, true, '5-8') will return all leads which have quality rating of 5, 6, 7 or 8.

        Setting the $limit parameter allows you to limit the maximum number of results.
        Calling lead_search('', null, true, null, 10); will return a maximum of 10 leads.
        Calling lead_search('', null, true, null, '10, 15'); will return a maximum of 10 leads, starting from lead number 15.
        Thus, the $limit parameter works identical to MySQL's LIMIT clause.
*/

    public function lead_search($search_phrase = '', $tags_arr = null, $return_full_data = true, $quality_rating = null, $limit = null)
    {
        if (is_array($tags_arr)) $tags_arr = serialize($tags_arr);

        $res = $this->http_get('leads',
                                array('search'    => $search_phrase,
                                      'tags'      => $tags_arr,
                                      'full_data' => $return_full_data,
                                      'quality'   => $quality_rating,
                                      'limit'     => $limit));

        return $res;
    }



    // Returns the number of API calls remaining for the current user
    // The current user is defined by the supplied $api_key while constructing the class instance
    public function get_remaining_api_calls()
    {
        // Refresh the $this->api_calls_remaining value if needed:
        if ($this->api_calls_last_checked == 0 or
            microtime(true) - $this->api_calls_last_checked > 15) $this->status();

        return $this->api_calls_remaining;
    }


    // Returns the changelog of the Verifigator API
    // If $version input parameter is given, the response will contain the
    // changes of only the requested API version. If not given, full change log is returned
    public function changelog_api($version = null)
    {
        $res = $this->http_get('changelog/api', array('version' => $version));
        return $res;
    }

    // Returns the changelog of the Verifigator API PHP Integration class
    // If $version input parameter is given, the response will contain the
    // changes of only the requested class version. If not given, full change log is returned
    // Note: This function can also be used to check what is the most recent version of the class
    public function changelog($version = null)
    {
        $res = $this->http_get('changelog/integrator_php', array('version' => $version));
        return $res;
    }

}
